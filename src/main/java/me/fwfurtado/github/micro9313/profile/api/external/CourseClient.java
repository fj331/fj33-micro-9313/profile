package me.fwfurtado.github.micro9313.profile.api.external;

import me.fwfurtado.github.micro9313.profile.api.views.CourseView;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import static me.fwfurtado.github.micro9313.profile.api.external.CourseClient.CourseClientFallback;

@Primary
@FeignClient(name = "course", fallback = CourseClientFallback.class)
public interface CourseClient {
    @GetMapping("/courses/{slug}")
    CourseView findBySlug(@PathVariable String slug);


    @Component
    class CourseClientFallback implements CourseClient {
        @Override
        public CourseView findBySlug(String slug) {
            return new CourseView("unknown-id", "Unknown Title");
        }
    }
}
