package me.fwfurtado.github.micro9313.profile.api.external;

import me.fwfurtado.github.micro9313.profile.api.ProfileController;
import me.fwfurtado.github.micro9313.profile.api.views.UserPublicView;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@ProfileController
class PublicProfileController {

    private final PublicProfileService service;

    PublicProfileController(PublicProfileService service) {
        this.service = service;
    }

    @GetMapping("{id}")
    UserPublicView show(@PathVariable Long id) {
        return service.showUserBy(id);
    }
}
