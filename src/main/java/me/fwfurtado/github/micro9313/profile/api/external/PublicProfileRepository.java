package me.fwfurtado.github.micro9313.profile.api.external;

import me.fwfurtado.github.micro9313.profile.shared.Profile;

import java.util.Optional;

public interface PublicProfileRepository {
    Optional<Profile> findById(Long id);
}
