package me.fwfurtado.github.micro9313.profile.api.external;

import me.fwfurtado.github.micro9313.profile.api.views.UserPublicView;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
class PublicProfileService {

    private final CourseClient courseClient;
    private final PublicProfileRepository repository;

    PublicProfileService(CourseClient courseClient, PublicProfileRepository repository) {
        this.courseClient = courseClient;
        this.repository = repository;
    }

    public UserPublicView showUserBy(Long id) {
        var user = repository.findById(id).orElseThrow();
        var course = courseClient.findBySlug("java-soa-integration");
        return new UserPublicView(user.getName(), user.getBio(), List.of(course));
    }
}
