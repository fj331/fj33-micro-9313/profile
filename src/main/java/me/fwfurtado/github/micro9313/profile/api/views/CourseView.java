package me.fwfurtado.github.micro9313.profile.api.views;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public record CourseView(
        @JsonProperty String id,
        @JsonProperty String title
) {
    @JsonCreator
    static CourseView of(String id, String title) {
        return new CourseView(id, title);
    }
}
