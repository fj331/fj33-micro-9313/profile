package me.fwfurtado.github.micro9313.profile.api.views;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public record UserPublicView(
        @JsonProperty String name,
        @JsonProperty String bio,
        @JsonProperty("completed_courses") List<CourseView> completedCourses

) {
}
