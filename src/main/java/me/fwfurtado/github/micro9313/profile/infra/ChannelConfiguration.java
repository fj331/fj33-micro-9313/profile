package me.fwfurtado.github.micro9313.profile.infra;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBinding(UserChannel.class)
class ChannelConfiguration {

}
