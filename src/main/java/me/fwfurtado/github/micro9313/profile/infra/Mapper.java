package me.fwfurtado.github.micro9313.profile.infra;

public interface Mapper<S, T> {
    T map(S source);
}
