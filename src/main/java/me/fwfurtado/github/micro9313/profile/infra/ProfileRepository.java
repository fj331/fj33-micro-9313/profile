package me.fwfurtado.github.micro9313.profile.infra;

import me.fwfurtado.github.micro9313.profile.api.external.PublicProfileRepository;
import me.fwfurtado.github.micro9313.profile.listeners.CreateProfileUserRepository;
import me.fwfurtado.github.micro9313.profile.shared.Profile;
import org.springframework.data.repository.Repository;

interface ProfileRepository extends Repository<Profile, Long>, PublicProfileRepository, CreateProfileUserRepository {
}
