package me.fwfurtado.github.micro9313.profile.infra;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface UserChannel {
    String CHANNEL_NAME = "users";

    @Input(CHANNEL_NAME)
    SubscribableChannel channel();
}
