package me.fwfurtado.github.micro9313.profile.listeners;

import me.fwfurtado.github.micro9313.profile.shared.Profile;

public interface CreateProfileUserRepository {
    void save(Profile profile);
    boolean existsProfileById(Long id);
}
