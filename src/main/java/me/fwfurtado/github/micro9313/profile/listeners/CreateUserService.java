package me.fwfurtado.github.micro9313.profile.listeners;

import org.springframework.stereotype.Service;

@Service
class CreateUserService {


    private final CreatedUserMapper profileMapper;
    private final CreateProfileUserRepository repository;

    CreateUserService(CreatedUserMapper profileMapper, CreateProfileUserRepository repository) {
        this.profileMapper = profileMapper;
        this.repository = repository;
    }

    public void createProfileBy(CreatedUserEvent event) {
        var profile = profileMapper.map(event);


        if (!repository.existsProfileById(profile.getId())) {
            repository.save(profile);
        }
    }
}
