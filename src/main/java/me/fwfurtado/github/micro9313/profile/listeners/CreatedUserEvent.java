package me.fwfurtado.github.micro9313.profile.listeners;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

record CreatedUserEvent(
        @JsonProperty Long id,
        @JsonProperty String name
) {

    @JsonCreator
    static CreatedUserEvent of(Long id, String name) {
        return new CreatedUserEvent(id, name);
    }
}
