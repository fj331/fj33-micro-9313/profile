package me.fwfurtado.github.micro9313.profile.listeners;

import me.fwfurtado.github.micro9313.profile.infra.Mapper;
import me.fwfurtado.github.micro9313.profile.shared.Profile;
import org.springframework.stereotype.Component;

@Component
public class CreatedUserMapper implements Mapper<CreatedUserEvent, Profile> {
    @Override
    public Profile map(CreatedUserEvent source) {
        return new Profile(source.id(), source.name());
    }
}
