package me.fwfurtado.github.micro9313.profile.listeners;

import me.fwfurtado.github.micro9313.profile.infra.UserChannel;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;

@Component
class UserListener {


    private final CreateUserService service;

    UserListener(CreateUserService service) {
        this.service = service;
    }

    @StreamListener(value = UserChannel.CHANNEL_NAME)
    void handle(CreatedUserEvent event) {
        service.createProfileBy(event);
    }

}
