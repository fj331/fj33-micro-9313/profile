package me.fwfurtado.github.micro9313.profile.shared;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "profiles")
public class Profile {
    @Id
    private Long id;

    private String name;
    private String bio;

    @ElementCollection
    private Set<Long> completedCourses = new HashSet<>();

    protected Profile() {
    }

    public Profile(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getBio() {
        return bio;
    }

    public Set<Long> getCompletedCourses() {
        return completedCourses;
    }

    public void addCourse(Long id) {
        completedCourses.add(id);
    }
}
