package me.fwfurtado.github.micro9313.profile.api.external;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@AutoConfigureStubRunner(ids = "me.github.fwfurtado.micro9313:course:+:stubs:9126", stubsMode = StubRunnerProperties.StubsMode.LOCAL)
class CourseClientTest {

    @Autowired
    private CourseClient client;


    @Test
    void shouldGetCourseBySlug() {
        var course = client.findBySlug("java-soa-integration");

        assertEquals("java-soa-integration", course.id());
        assertEquals("Java Soa Integration", course.title());
    }

}