package me.fwfurtado.github.micro9313.profile.listeners;

import me.fwfurtado.github.micro9313.profile.IntegrationTest;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.cloud.contract.stubrunner.StubTrigger;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.then;

@IntegrationTest
@AutoConfigureStubRunner(ids="me.github.fwfurtado.micro9313:user:+:stubs:9012", stubsMode = StubRunnerProperties.StubsMode.LOCAL)
class UserListenerTest {

    @Autowired
    private StubTrigger trigger;

    @SpyBean
    private UserListener listener;

    @Captor
    private ArgumentCaptor<CreatedUserEvent> eventArgumentCaptor;

    @Test
    void shouldReceiveEvent() {
        trigger.trigger("created_user");

        then(listener).should().handle(eventArgumentCaptor.capture());

        var event = eventArgumentCaptor.getValue();
        assertEquals(1L, event.id());
        assertEquals("Fernando Furtado", event.name());
    }

}